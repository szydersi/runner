﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour {

	public float speed;
	Rigidbody rb;
	bool rightDirection;
	Vector3 direction;
    public float nextSpawnTime;
    float score, lastScore;
    

	void Start () {
		rb = GetComponent<Rigidbody> ();
		rightDirection = true;
	}


	void Update () {
        score += Time.deltaTime;
        nextSpawnTime = 3f / speed;
        //Zwiekszanie predkosci wraz z uplywem czasu
		speed = speed + Time.deltaTime /5;

        //Sterowanko
		if (Input.GetKeyDown (KeyCode.Space) && rightDirection == true) {
			rightDirection = false;
		}
		else if (Input.GetKeyDown (KeyCode.Space) && rightDirection == false) {
			rightDirection = true;
		} 
        

		if (rightDirection == true) {
            direction = new Vector3 (1, 0, 0).normalized;
           // rb.AddForce(new Vector3(speed, 0, 0));
		} 
		if (rightDirection == false) {
            direction = new Vector3 (-1, 0, 0).normalized;
           // rb.AddForce(new Vector3(-speed, 0, 0));
        }

		Vector3 velocity = direction * speed * Time.deltaTime;
		transform.Translate (velocity);

        Respawn();
        
	}

    //Funkcja resetowania poziomu
    void Respawn() {
        if (transform.position.y <= -5f)
        {
            score = 0;
            SceneManager.LoadScene("firstStage");
        }
    }


    /*	void OnCollisionStay(Collision col){

        }
        void OnCollisionExit(Collision col){

        }
    */


    private void OnGUI()
    {
        GUI.Label(new Rect(Camera.main.pixelWidth / 5 - 100, 25, 100, 50), "Score : " + score);
    }
}
