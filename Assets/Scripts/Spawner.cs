﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject groundPrefab;
	int random;
	float licznik;
	Vector2 screenHalfSizeWorldUnits;
    public float nextSpawnTime;
    //Zmienne pierwszego bloczka
    Vector2 firstSpawnPosition = new Vector2(0, -1);
    bool firstSpawned = false;
    Vector2 nextSpawnPosition;

    //Prefab gracza 
    public GameObject player;
	PlayerScript ps;

	void Start () {
		ps = player.GetComponent<PlayerScript> ();
        nextSpawnTime = ps.nextSpawnTime ;
	}
	

	void Update () {

        nextSpawnTime -= Time.deltaTime;
        if(nextSpawnTime <= 0 && firstSpawned == false)
        {
            Instantiate(groundPrefab, new Vector2(player.transform.position.x + 2f, -1f), Quaternion.identity);
            firstSpawned = true;
            nextSpawnPosition = new Vector2(firstSpawnPosition.x + 2f, firstSpawnPosition.y);
        }else if (nextSpawnTime <= 0 && firstSpawned == true)
		 {
            random = Random.Range(0, 2);
            Debug.Log(random);
			if (random == 0) {
				Instantiate (groundPrefab, new Vector2 (player.transform.position.x + 2f, -1f), Quaternion.identity);
			}else Instantiate (groundPrefab, new Vector2 (player.transform.position.x - 2f, -1f), Quaternion.identity);

            nextSpawnTime = ps.nextSpawnTime;
		}
	}
}
