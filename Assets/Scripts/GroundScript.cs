﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour {

    float timeToNextSpawn;
    float timeToDestroy;
    GameObject player;
    public GameObject groundPrefab;
    PlayerScript ps;
    void Start()
    {
        player = GameObject.Find("Player");
        ps = player.GetComponent<PlayerScript>();
        timeToDestroy = 10f / ps.speed  ;
        timeToNextSpawn = 2f/ ps.speed ;
    }

    // Update is called once per frame
    void Update()
    {
        timeToDestroy -= Time.deltaTime;
        timeToNextSpawn -= Time.deltaTime;
     //   Debug.Log(timeToNextSpawn);

        if (timeToNextSpawn <= 0)
        {
            int random = Random.Range(0, 2);
            if (random == 0)
            {
                Instantiate(groundPrefab, new Vector2(transform.position.x + 2f, -1f), Quaternion.identity);
                timeToNextSpawn = 99f;
            }
            else if(random == 1)
            {
                Instantiate(groundPrefab, new Vector2(transform.position.x - 2f, -1f), Quaternion.identity);
                timeToNextSpawn = 99f;
            }
        }

        if (timeToDestroy <= 0)
        {
            Destroy(gameObject);
        }
    }
}
