﻿using UnityEngine;
using System.Collections;

public class GroundDestroyer : MonoBehaviour {

	float timer;
	GameObject player;
	PlayerScript ps;
	void Start () {
		player = GameObject.Find ("Player");
		ps = player.GetComponent<PlayerScript> ();
        timer = ps.nextSpawnTime + 1f;
    }
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime ;
		if (timer <= 0) {
			Destroy (gameObject);
		}
	}
}
