﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstFloorScript : MonoBehaviour {

    float timer;
	// Use this for initialization
	void Start () {
        timer = 4f;

    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }

}
